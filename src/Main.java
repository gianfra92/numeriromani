
import java.util.Arrays;

import javax.swing.*;

public class Main {	
	
	public static void main(String[] args) {		
		
		//Crea una finestra di dialogo con input per l'utente
		String num = JOptionPane.showInputDialog(null,"Inserisci un Numero","Numeri Romani",3);
		
		//Verifica che non abbia annullato
		if (num == null) {
			System.out.println("Annullato");
			return;
		}
		
		//Stampa del risultato
		System.out.println(romanToInt(num));
	}
	
	
	public static int romanToInt(String rNum) {
		
		//inizializazione delle variabili
		char[] rSymbol = { 	'I',
							'V',
							'X',
							'L',
							'C',
							'D',
					'M'	};
		int[] rValue = {	1,
							5,
							10,
							50,
							100,
							500,
							1000};		
		
		int tRoman[] = new int[rNum.length()];
		int roman = 0;
		
		//check if the input is not empty
		if (rNum.length()==0) throw new IllegalArgumentException("String empty");
		
		//Crea un array di valori int equivalenti ai simboli inseriti
		for (int j=0; j<rNum.length();j++) {
			for (int i=0;i<rSymbol.length;i++) {	
				
				if (rNum.charAt(j)==rSymbol[i]) {
					tRoman[j]=rValue[i];
					break;
				}
				
				//verifica che i numeri romani siano scritti correttamente
				if (i==rSymbol.length-1) throw new IllegalArgumentException("String not recognized");
				
			}
			
		}
		
		//somma e calcola il risultato
		for (int j=0; j<tRoman.length-1;j++) {
			if (tRoman[j]>=tRoman[j+1]) roman += tRoman[j];
			else roman -= tRoman[j];
		}
		roman +=tRoman[tRoman.length-1];
		
		//Stampa info utili sui numeri
		System.out.println("Number in Roman: "+rNum);
		System.out.println("Roman Array in int: " + Arrays.toString(tRoman));
		System.out.println("Roman Value: " + roman);
		
		return roman;
	}
}
